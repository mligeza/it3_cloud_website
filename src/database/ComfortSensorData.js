export const comfortSensorDataJSON = 
    {
        
        "CSID": {
            "S": "24"
        },
        "co2": {
            "N": "3104"
        },
        "humidity": {
            "N": "47"
        },
        "pm10": {
            "N": "25.381555170070406"
        },
        "pm25": {
            "N": "0.7899213990957743"
        },
        "temperature": {
            "N": "21"
        },
        "timestamp": {
            "N": "1611075433129"
        },
        "voc": {
            "N": "234"
        }
    }