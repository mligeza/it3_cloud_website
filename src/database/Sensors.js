export const sensorJSON = [
    {
        SID: {
            S:    10
        },
        description: {
            S:    "HQ"
        },
        gateway: {
            S:    10
        },
        position:{
            M: {  x : { N : 49.71145 },  y : { N : 20.93687 }}
        },
        type:{
            S:    "gateway"
        }
    },{
        SID: {
            S:    13
        },
        description: {
            S:    "HQ comfort sensor"
        },
        gateway: {
            S:    10
        },
        position:{
            M: {  x : { N : 49.71140 },  y : { N : 20.93680 }}
        },
        type:{
            S:    "comfort"
        }
    },{
        SID: {
            S:    20
        },
        description: {
            S:    "Gateway for Krakow hall"
        },
        gateway: {
            S:    20
        },
        position:{
            M: {  x : { N : 50.0227 },  y : { N : 20.0575 }}
        },
        type:{
            S:    "gateway"
        }
    },{
        SID: {
            S:    21
        },
        description: {
            S:    "Dragon fire; Krakow production hall"
        },
        gateway: {
            S:    20
        },
        position:{
            M: {  x : { N : 50.0229 },  y : { N : 20.0575 }}
        },
        type:{
            S:    "machine"
        } 
    },{
        SID: {
            S:    22
        },
        description: {
            S:    "Sheep spawner; Krakow production hall"
        },
        gateway: {
            S:    20
        },
        position:{
            M: {  x : { N : 50.0225 },  y : { N : 20.0575 }}
        },
        type:{
            S:    "machine"
        }
    },{
        SID: {
            S:    23
        },
        description: {
            S:    "Main hall; Krakow production hall"
        },
        gateway: {
            S:    20
        },
        position:{
            M: {  x : { N : 50.0227 },  y : { N : 20.0595 }}
        },
        type:{
            S:    "comfort"
        }
    },{
        SID: {
            S:    24
        },
        description: {
            S:    "The Tower; Krakow production hall"
        },
        gateway: {
            S:    20
        },
        position:{
            M: {  x : { N : 50.0227 },  y : { N : 20.0555 }}
        },
        type:{
            S:    "comfort"
        }
    },{
        SID: {
            S:    30
        },
        description: {
            S:    "Gateway for Bydgoszcz hall"
        },
        gateway: {
            S:    30
        },
        position:{
            M: {  x : { N : 53.12336 },  y : { N : 18.07481 }}
        },
        type:{
            S:    "gateway"
        }
    },{
        SID: {
            S:    31
        },
        description: {
            S:    "Main Laser; Bydgoszcz production hall"
        },
        gateway: {
            S:    30
        },
        position:{
            M: {  x : { N : 53.12276 },  y : { N : 18.07481 }}
        },
        type:{
            S:    "machine"
        }
    },{
        SID: {
            S:    32
        },
        description: {
            S:    "Clone producer; Bydgoszcz production hall"
        },
        gateway: {
            S:    30
        },
        position:{
            M: {  x : { N : 53.12380 },  y : { N : 18.07481 }}
        },
        type:{
            S:    "machine"
        }
    },{
        SID: {
            S:    33
        },
        description: {
            S:    "Main hall, for Bydgoszcz production hall"
        },
        gateway: {
            S:    30
        },
        position:{
            M: {  x : { N : 53.12305 },  y : { N : 18.07481 }}
        },
        type:{
            S:    "comfort"
        }
    },{
        SID: {
            S:    34
        },
        description: {
            S:    "Office, for Bydgoszcz production hall"
        },
        gateway: {
            S:    30
        },
        position:{
            M: {  x : { N : 53.12336 },  y : { N : 18.07550 }}
        },
        type:{
            S:    "comfort"
        }
    }


]