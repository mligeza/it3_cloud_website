import React from "react";
import './Sensor.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

function createData(name, value) {
  return { name, value };
}

export class SensorView extends React.Component {
  constructor(props){
    super(props); 
    //console.log(props);
    this.state= {
      rows:[
        createData("SID", props.SID),
        createData("GWID", props.GWID),
        createData("Type", props.type),
        createData("Description", props.description),
        createData("Position", JSON.stringify(props.position))
      ]
       
    };
    
  }
  
   

  render() {
    //TODO: add table with data about sensor
      return (
        <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableBody>
            {this.state.rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.value}</TableCell>
                
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>);
      
    }
  }
