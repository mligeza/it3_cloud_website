import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import './Map.css';
import 'leaflet/dist/leaflet.css';
import axios from 'axios';

export class MyMap extends React.Component {
  constructor(props){
    super(props); 
    var mark = this.parseSensorToMarker(this.props.table);
    this.state= {
      sensors: [this.props.table],
      markers: mark, 
      geoUrl: "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json",
      app: this.props.app
    };
    ////console.log(this.state.markers);
    ////console.log(this.props.table);
  }
  
   parseSensorToMarker(sensor){
    var mark =[];
    sensor.forEach(element => {
      ////console.log(element)
      mark.push({
        id: element.SID,
        type:element.type,
        description: element.description,
        //coordinates: {x: element.position.M.x.N, y:element.position.M.y.N}});
        coordinates: {x: element.position.x, y:element.position.y}});
    });
    return mark;
  }
/*
  shouldComponentUpdate(nextProps){
    return nextProps.sensors !== this.state.table;
}

  componentDidUpdate(prevProps){
    if(prevProps.sensors !== this.props.table){
      var mark = this.parseSensorToMarker(this.props.table);
        this.setState({          
          sensors: this.props.sensors,
          markers: mark
        });
    }
    //console.log(" MAP: "+JSON.stringify(this.state.markers))
}
*/
componentDidMount(){
  axios
      .post("https://llhfw04rc2.execute-api.us-east-1.amazonaws.com/test-1/sensors", {})
      .then(response => {
        var s=response.data.Items
        ////console.log("s:" + JSON.stringify(s)) 
          this.setState({sensors: s}) 

          ////console.log("sensors state:" + JSON.stringify(this.state.sensors)) 
          var mark = this.parseSensorToMarker(this.state.sensors);
          this.setState({markers: mark}) 
          ////console.log("Markers:" + JSON.stringify(this.state.markers)) 
          this.state.app.setSensors(s)
         })
      .catch(error =>{
        //console.log(error)
      } 
        );
     
}

  render() {
    
      return (
      <div id="map">
        
       
         <h1>Sensor map</h1>
        
         <MapContainer center={[50.0227, 20.057]} zoom={16} scrollWheelZoom={false}
        
         style={{ height: "50vh", width: "100%" }}
         >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {
          
          this.state.markers.map((element) =>
            
            ////console.log(JSON.stringify(element.id) +" "+JSON.stringify(element.coordinates.x) +" "+JSON.stringify(element.coordinates.y) +" "+JSON.stringify(element.description))
            <Marker  key={element.id} position={[element.coordinates.x, element.coordinates.y]}  eventHandlers={{
              click: () => {
                
                //console.log('marker clicked: ' + element.id)
                this.state.app.sensorClicked(element.id)
              },
            }}>
             
              <Popup eventHandlers={{
          click: () => {
            
            //console.log("Emptness clicked");
            this.state.app.sensorClicked(null)
          },
        }}> 
                {JSON.stringify(element.description)}
              </Popup>
            </Marker>
          ) 
         
          }
          
        </MapContainer>
       
    
       
      </div>);
      
    }
  }
