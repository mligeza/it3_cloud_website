import React from "react";
//import './ComfortSensor.css';


import {comfortSensorDataJSON} from "./../database/ComfortSensorData";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



function createData(name, value) {
  return { name, value };
}


//TODO: ComfortSensorData
export class ComfortSensorDataView extends React.Component {

  

  constructor(props){
    super(props); 
    //console.log(props);
    var id= props.CSID;

      ////console.log(el.timestamp.N)
      var t=comfortSensorDataJSON.timestamp.N;
      const milliseconds = (t-(t%1))/1;
      ////console.log(milliseconds);
      const dateObject = new Date(milliseconds)
      const humanDateFormat = dateObject.toLocaleString() //2019-12-9 10:30:15
      var date=humanDateFormat;
  
    


    this.state= {
      rows: [
        createData("CSID", props.CSID),
        createData("Time", date),

        createData("Temperature", props.temperature),
        createData("CO2", props.co2),
        createData("Humidity", props.humidity),
        createData("PM 10", props.pm10),
        createData("PM 2.5", props.pm25),
        createData("VOC", props.voc),
      ]
    };
   ////console.log(this.state)
  }

   

   

  render() {
  
        return (
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableBody>
                {this.state.rows.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.value}</TableCell>
                    
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        );
      
        }
  }
