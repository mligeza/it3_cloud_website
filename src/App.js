import './App.css';
import { sensorJSON } from './database/Sensors'; 
import {MyMap} from './components/Map';
import React, { Component } from "react";
import {ComfortSensorDataView} from "./components/ComfortSensorData"
import {MachineSensorDataView} from "./components/MachineSensorData"
import {SensorView} from './components/Sensor'
import { AmplifyAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import axios from 'axios';



class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      //sensors: sensorJSON,
      sensors: [],
      type: null,
      id: null,
      sensor: {}
    }
    ////console.log(sensorJSON);
  }
  setSensors(s){
    this.setState({sensors:s})
  }

  sensorClicked(id){
    
    
    if(id==null){this.setState({type: null})}
    else{
      this.setState({type: null})
      ////console.log(this.state.sensors);
      let newType=null;
      this.state.sensors.forEach(element => {
        if(element.SID == id)
        {
          newType= element.type;
        }
      });


      axios
      .post("https://llhfw04rc2.execute-api.us-east-1.amazonaws.com/test-1/sensor",  {
        "SID": id
        }, {
          //headers: headers
        })
      .then(response => {
        ////console.log(response.data)
        this.setState({sensor:response.data,id:id, type:newType})
        
        //console.log("Got sensor:" + this.state.id);
        //console.log("Sensor type:" + this.state.type);
        sensorJSON.forEach(element => {
          if(element.SID.S == id){
            this.setState({type: element.type.S});
          }
            //if(this.state.type="")
            ////console.log("Got sensor type:" + this.state.type);
          });
        //});

      })
      .catch(error => console.log(error));

     
     
    
      }
    }

  componentDidMount () {
    const leafletcss = document.createElement("link");
    leafletcss.rel="stylesheet";
    leafletcss.href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css";
    //leafletcss.integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==";
    leafletcss.crossorigin="";

    const script = document.createElement("script");
    script.src = "https://unpkg.com/leaflet@1.7.1/dist/leaflet.js";
    //script.integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    script.crossorigin="";

    document.head.appendChild(leafletcss);
    document.head.appendChild(script);

    
  }

  render(){
    let sensor;
    if(this.state.type=="comfort"){
        sensor= <ComfortSensorDataView 
        CSID={this.state.sensor.CSID}
        timestamp={this.state.sensor.timestamp}
        temperature={this.state.sensor.temperature}
        co2={this.state.sensor.co2}
        humidity={this.state.sensor.humidity}
        pm10={this.state.sensor.pm10}
        pm25={this.state.sensor.pm25}
        voc={this.state.sensor.voc}
        />
    } else if (this.state.type=="gateway"){
      sensor= <SensorView SID={this.state.sensor.SID} 
      GWID={this.state.sensor.GWID} 
      type={this.state.sensor.type} 
      description={this.state.sensor.description} 
      position={this.state.sensor.position}/>
    }else if (this.state.type=="machine"){
      sensor= <MachineSensorDataView 
      MSID={this.state.sensor.MSID} 
      radon={this.state.sensor.radon} 
      temperature={this.state.sensor.temperature} 
      power={this.state.sensor.power}
      timestamp={this.state.sensor.timestamp}
      />
    }
    
  return (
    <AmplifyAuthenticator>
    <div className="App">
      <header className="App-header">
        <MyMap table={this.state.sensors} app={this} />
        <div id="mapid"  height="100px" />
        {sensor}
      </header>
      <AmplifySignOut />
    </div>
    </AmplifyAuthenticator>
  );
}
}
export default App;
//export default withAuthenticator(App, true);
